+++
date = "2016-03-06T23:34:32-08:00"
draft = false
title = "about"

+++

## A headline

Hey, this is my post!

~~~html
<section id="main">
  <div>
    <h1 id="title">{{ .Title }}</h1>
    {{ range .Data.Pages }}
      {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
~~~
